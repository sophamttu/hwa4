import java.util.*;
import static java.lang.Math.abs;
import static java.lang.Math.*;

/** This class represents fractions of form n/d where n and d are long integer 
 * numbers. Basic operations and arithmetics for fractions are provided.
 */
public class Lfraction implements Comparable<Lfraction> {


   /** Main method. Different tests. */
   public static void main (String[] param) {
   }

   private long numerator;
   private long denominator;
   /** Constructor.
    * @param a numerator
    * @param b denominator > 0
    */
   public Lfraction (long a, long b) {
      if(b <= 0)
         throw new IllegalArgumentException("denominator must be a non-zero positive number");
      numerator = a;
      denominator = b;
      this.reduce();
   }

   /** Public method to access the numerator field. 
    * @return numerator
    */
   public long getNumerator() {
      return numerator;
   }

   /** Public method to access the denominator field. 
    * @return denominator
    */
   public long getDenominator() {
      return denominator;
   }

   /** Conversion to string.
    * @return string representation of the fraction
    */
   @Override
   public String toString() {
      return String.valueOf(numerator) + '/' + denominator;
   }

   public static long gcd(long a, long b) {
      long max = max(a,b);
      long min = min(a,b);
      if(min == 0) return max;
      if(max % min != 0 || gcd(min, max % min) != 1) {
         return gcd(min, max % min);
      }
      return min;
   }

   public void reduce() {
      long gcd = gcd(abs(numerator), denominator);
      numerator /= gcd;
      denominator /= gcd;
   }
   /** Equality test.
    * @param m second fraction
    * @return true if fractions this and m are equal
    */
   @Override
   public boolean equals (Object m) {
      if(m.getClass() != this.getClass()) return false;
      return this.compareTo((Lfraction) m) == 0;
   }

   /** Hashcode has to be equal for equal fractions.
    * @return hashcode
    */
   @Override
   public int hashCode() {
      return Objects.hash(this.numerator, this.denominator);
   }

   /** Sum of fractions.
    * @param m second addend
    * @return this+m
    */
   public Lfraction plus (Lfraction m) {
      Lfraction result = new Lfraction(this.numerator*m.denominator + m.numerator*this.denominator,this.denominator * m.denominator);
      return result;
   }

   /** Multiplication of fractions.
    * @param m second factor
    * @return this*m
    */
   public Lfraction times (Lfraction m) {
      Lfraction result = new Lfraction(this.numerator*m.numerator,this.denominator*m.denominator);
      return result;
   }

   /** Inverse of the fraction. n/d becomes d/n.
    * @return inverse of this fraction: 1/this
    */
   public Lfraction inverse() {
      if(this.numerator == 0)
         throw new IllegalArgumentException("division by 0");
      Lfraction result = new Lfraction(this.denominator,abs(this.numerator));
      return (this.numerator>0)?result:result.opposite();
   }

   /** Opposite of the fraction. n/d becomes -n/d.
    * @return opposite of this fraction: -this
    */
   public Lfraction opposite() {
      return new Lfraction(this.numerator*-1,this.denominator);
   }

   /** Difference of fractions.
    * @param m subtrahend
    * @return this-m
    */
   public Lfraction minus (Lfraction m) {
      return this.plus(m.opposite());
   }

   /** Quotient of fractions.
    * @param m divisor
    * @return this/m
    */
   public Lfraction divideBy (Lfraction m) {
      if(m.denominator == 0)
              throw new IllegalArgumentException("division by 0");
      return this.times(m.inverse());
   }

   /** Comparision of fractions.
    * @param m second fraction
    * @return -1 if this < m; 0 if this==m; 1 if this > m
    */
   @Override
   public int compareTo (Lfraction m) {
      Lfraction diff = this.minus(m);
      if(diff.numerator > 0) return 1;
      else if(diff.numerator < 0) return -1;
      else
         return 0;
   }

   /** Clone of the fraction.
    * @return new fraction equal to this
    */
   @Override
   public Object clone() throws CloneNotSupportedException {
      return new Lfraction(this.numerator,this.denominator);

   }

   /** Integer part of the (improper) fraction. 
    * @return integer part of this fraction
    */
   public long integerPart() {
      return numerator/denominator;
   }

   /** Extract fraction part of the (improper) fraction
    * (a proper fraction without the integer part).
    * @return fraction part of this fraction
    */
   public Lfraction fractionPart() {
      return (abs(this.numerator) > this.denominator)?new Lfraction((this.numerator % this.denominator), this.denominator):this;
   }

   /** Approximate value of the fraction.
    * @return numeric value of this fraction
    */
   public double toDouble() {
      return (double)this.numerator/this.denominator;
   }

   /** Double value f presented as a fraction with denominator d > 0.
    * @param f real number
    * @param d positive denominator for the result
    * @return f as an approximate fraction of form n/d
    */
   public static Lfraction toLfraction (double f, long d) {
      return new Lfraction(Math.round(d * f), d);
   }

   /** Conversion from string to the fraction. Accepts strings of form
    * that is defined by the toString method.
    * @param s string form (as produced by toString) of the fraction
    * @return fraction represented by s
    */
   public static Lfraction valueOf (String s) {
      String[] ss = s.split("/");
      try {
         return new Lfraction(Long.parseLong(ss[0]),Long.parseLong(ss[1]));
      } catch(Exception e) {
         throw new IllegalArgumentException("illegal argument: " + s);
      }
   }
}

